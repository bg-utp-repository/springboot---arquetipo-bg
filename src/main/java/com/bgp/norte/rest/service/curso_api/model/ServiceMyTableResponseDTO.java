package com.bgp.norte.rest.service.curso_api.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Objeto que representa el cuerpo de la respuesta de ejemplo")
public class ServiceMyTableResponseDTO implements Serializable {
	private static final long serialVersionUID = -4819639784235761061L;
	private List<TableCatalogDTO> autorizedTables;

}
