package com.bgp.norte.rest.service.curso_api.generic.catalog.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bgp.norte.rest.service.curso_api.generic.catalog.GenericQueryAuthorizationWsClient;
import com.bgp.norte.rest.service.curso_api.generic.catalog.model.GenericRoleTableDTO;
import com.bgp.norte.rest.service.curso_api.generic.catalog.param.SPMaintenanceParam;
import com.bgp.norte.ws.client.rest.generic.dto.HeaderDTO;
import com.bgp.norte.ws.client.rest.generic.dto.VendorDB;
import com.bgp.norte.ws.client.rest.generic.enums.ConnectionName;
import com.bgp.norte.ws.client.rest.generic.enums.GenericParams;
import com.bgp.norte.ws.client.rest.generic.exception.BGPWsClientException;
import com.bgp.norte.ws.client.rest.generic.service.GenericWsClient;
import com.bgp.norte.ws.client.rest.generic.util.HeaderUtils;
import com.bgp.norte.ws.client.rest.generic.util.ParameterUtil;
import com.bgp.norte.ws.client.rest.generic.util.ResponseUtil;

import generated.bg.EjecutarProcedimientoRequest;
import generated.bg.EjecutarProcedimientoResponse;
import lombok.extern.slf4j.Slf4j;

@Service("GenericQueryAuthorizationWsClient")
@Slf4j
public class GenericQueryAuthorizationWsClientImpl implements GenericQueryAuthorizationWsClient {
	@Autowired
	private GenericWsClient genericWsClient;
	@Value("${generic.conector.dbNameLR}")
	private String dbName;

	@Override
	public List<GenericRoleTableDTO> queryAuthorization(final HeaderDTO header, String networkUser)
			throws BGPWsClientException {
		log.info("ejecutando queryAuthorization");

		List<GenericRoleTableDTO> response = null;

		// Logica de invocacion al respectivo generico
		final EjecutarProcedimientoRequest sol = new EjecutarProcedimientoRequest();
		setConnection(sol);

		HeaderUtils.setLocalHeader(header);
		ParameterUtil.addParameter(GenericParams.OPERATION, OPERATION_Q1, sol);
		ParameterUtil.addParameter(SPMaintenanceParam.NETWORK_USER, networkUser, sol);

		try {
			final EjecutarProcedimientoResponse respuesta = genericWsClient.invokeGenericService(sol,
					SP_BG_FIND_AUTHORIZED_TABLE);
			response = ResponseUtil.mapResult(respuesta.getResultSet().get(0), GenericRoleTableDTO.class);
		} catch (BGPWsClientException e) {
			log.error("Se ha presentado el siguiente error", e);
			throw e;
		}

		return response;
	}

	@Override
	public List<GenericRoleTableDTO> validateAccessToTable(final HeaderDTO header, String table, String networkUser)
			throws BGPWsClientException {
		log.info("ejecutando validateAccessToTable");

		List<GenericRoleTableDTO> response = null;

		// Logica de invocacion al respectivo generico
		final EjecutarProcedimientoRequest sol = new EjecutarProcedimientoRequest();
		setConnection(sol);

		HeaderUtils.setLocalHeader(header);
		ParameterUtil.addParameter(GenericParams.OPERATION, OPERATION_Q2, sol);
		ParameterUtil.addParameter(SPMaintenanceParam.NETWORK_USER, networkUser, sol);
		ParameterUtil.addParameter(SPMaintenanceParam.TABLE, table, sol);

		try {
			final EjecutarProcedimientoResponse respuesta = genericWsClient.invokeGenericService(sol,
					SP_BG_FIND_AUTHORIZED_TABLE);
			response = ResponseUtil.mapResult(respuesta.getResultSet().get(0), GenericRoleTableDTO.class);
		} catch (BGPWsClientException e) {
			log.error("Se ha presentado el siguiente error", e);
			throw e;
		}

		return response;
	}

	/**
	 * Set the connection name, data base and data base type.
	 *
	 * @param sol the new connection
	 */
	private void setConnection(EjecutarProcedimientoRequest sol) {
		sol.setConnectionName(ConnectionName.SYBASE_CONN_KALEO.value());
		sol.setDataBase(dbName);
		sol.setDataBaseType(VendorDB.SYBASE.value());
	}
}