package com.bgp.norte.rest.service.curso_api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Clase para Productos de catalogo")
public class ServiceMyCatalogRequestDTO implements Serializable {
	static final long serialVersionUID = -6943626258655345447L;
	private String table;
}
