package com.bgp.norte.rest.service.curso_api.generic.catalog.model;

import java.io.Serializable;
import java.util.List;

import com.bgp.norte.ws.client.rest.generic.dto.GenericClassDTO;
import com.bgp.norte.ws.client.rest.generic.annotations.Column;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class GenericTableListDTO implements Serializable {

	private static final long serialVersionUID = -6792078100878917049L;
	private List<GenericTableDTO> authorizedTables;
}
