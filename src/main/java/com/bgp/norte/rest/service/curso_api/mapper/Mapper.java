package com.bgp.norte.rest.service.curso_api.mapper;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.bgp.norte.rest.service.curso_api.generic.catalog.model.GenericCatalogDTO;
import com.bgp.norte.rest.service.curso_api.generic.catalog.model.GenericTableDTO;
import com.bgp.norte.rest.service.curso_api.model.FullCatalogDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyTableResponseDTO;
import com.bgp.norte.rest.service.curso_api.model.TableCatalogDTO;



@Service
public class Mapper {

	public ServiceMyTableResponseDTO fromCLTableDTOtoTableCatalogDTO(ServiceMyTableResponseDTO serviceMyTablesResponseDTO,
			final List<GenericTableDTO> genericList) {

		List<TableCatalogDTO> list = null;

		if (null != genericList && CollectionUtils.isNotEmpty(genericList)) {

			list = new ArrayList<>();
			TableCatalogDTO table = null;

			for (GenericTableDTO generic : genericList) {
				table = new TableCatalogDTO();
				table.setId(generic.getTabla().trim());
				table.setName(generic.getDescripcion().trim());

				if (null != table.getName().trim()) {
					list.add(table);
				}
			}
		}

		serviceMyTablesResponseDTO.setAutorizedTables(list);
		return serviceMyTablesResponseDTO;
	}
	
	  public List<FullCatalogDTO> fromCLCatalogDTOtoFullCatalogDTO(
		      final List<GenericCatalogDTO> genericList) {
		    List<FullCatalogDTO> list = null;
		    if (null != genericList && CollectionUtils.isNotEmpty(genericList)) {
		      list = new ArrayList<>();
		      FullCatalogDTO catalogo = null;

		      for (GenericCatalogDTO generic : genericList) {
		        catalogo = new FullCatalogDTO();
		        catalogo.setId(generic.getId().trim());
		        catalogo.setName(generic.getName().trim());
		        catalogo.setStatus(generic.getStatus());
		        catalogo.setUpdated("N");
		        if (null != catalogo.getName().trim()) {
		          list.add(catalogo);
		        }
		      }
		    }
		    return list;
		  }
}

