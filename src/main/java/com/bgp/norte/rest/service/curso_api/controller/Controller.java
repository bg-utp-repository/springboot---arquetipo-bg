package com.bgp.norte.rest.service.curso_api.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bgp.norte.rest.service.common.entities.BgpRequestEntity;
import com.bgp.norte.rest.service.common.entities.BgpResponseEntity;
import com.bgp.norte.rest.service.common.exception.BgpRestBusinessException;
import com.bgp.norte.rest.service.common.util.ResponseUtils;
import com.bgp.norte.rest.service.curso_api.command.Command;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyCatalogRequestDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyCatalogResponseDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyCatalogUpdateRequestDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyTableRequestDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyTableResponseDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("${api.baseUrl}")
@Api(tags = { "catalog.maintenance" })
@Slf4j
public class Controller {
	@Autowired
	private Command command;

	@Autowired
	private ResponseUtils responseUtils;

	@PostMapping(path = "/find/getTables")
	@ApiOperation(value = "Busqueda de las tablas autorizadas al usuario para mantenimiento", httpMethod = "POST", nickname = "getTables", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public BgpResponseEntity<ServiceMyTableResponseDTO> getQueryTables(
			@Valid @RequestBody BgpRequestEntity<ServiceMyTableRequestDTO> request) throws BgpRestBusinessException {
		log.info("Ejecutando servicio REST getTables");

		final BgpResponseEntity<ServiceMyTableResponseDTO> response = command.queryTables(request);

		responseUtils.validateRestResponse(response);

		return response;
	}

	@PostMapping(path = "/find/getCatalog")
	@ApiOperation(value = "Busqueda de un catalogo completo", httpMethod = "POST", nickname = "getCatalog", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public BgpResponseEntity<ServiceMyCatalogResponseDTO> getQueryCatalog(
			@Valid @RequestBody BgpRequestEntity<ServiceMyCatalogRequestDTO> request) throws BgpRestBusinessException {
		log.info("Ejecutando servicio REST getCatalog");

		final BgpResponseEntity<ServiceMyCatalogResponseDTO> response = command.queryCatalog(request);

		responseUtils.validateRestResponse(response);

		return response;
	}

	@PostMapping(path = "/update/setCatalog", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Actualiza los detalles de un catalogo especifico", httpMethod = "POST", nickname = "setCatalog", produces = MediaType.APPLICATION_JSON_VALUE, response = ResponseEntity.class)
	public BgpResponseEntity<ServiceMyCatalogResponseDTO> updateCatalog(
			@RequestBody BgpRequestEntity<ServiceMyCatalogUpdateRequestDTO> request) throws BgpRestBusinessException {

		final BgpResponseEntity<ServiceMyCatalogResponseDTO> response = command.updateCatalog(request);

		responseUtils.validateRestResponse(response);

		return response;
	}
}
