package com.bgp.norte.rest.service.curso_api.generic.catalog;

import java.util.List;

import com.bgp.norte.rest.service.curso_api.generic.catalog.model.GenericRoleTableDTO;
import com.bgp.norte.ws.client.rest.generic.dto.HeaderDTO;
import com.bgp.norte.ws.client.rest.generic.exception.BGPWsClientException;

public interface GenericQueryAuthorizationWsClient {
	String OPERATION_Q1 = "Q1";
	String OPERATION_Q2 = "Q2";
	String SP_BG_FIND_AUTHORIZED_TABLE = "sp_bg_findautorizedtable";

	List<GenericRoleTableDTO> queryAuthorization(final HeaderDTO header, String networkUser) throws BGPWsClientException;
	
	List<GenericRoleTableDTO> validateAccessToTable(final HeaderDTO header, String table, String networkUser) throws BGPWsClientException;

}
