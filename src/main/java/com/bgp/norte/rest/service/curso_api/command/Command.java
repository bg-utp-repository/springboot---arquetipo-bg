package com.bgp.norte.rest.service.curso_api.command;

import com.bgp.norte.rest.service.common.entities.BgpRequestEntity;
import com.bgp.norte.rest.service.common.entities.BgpResponseEntity;
import com.bgp.norte.rest.service.common.exception.BgpRestBusinessException;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyCatalogRequestDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyCatalogResponseDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyCatalogUpdateRequestDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyTableRequestDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyTableResponseDTO;

public interface Command {
	BgpResponseEntity<ServiceMyTableResponseDTO> queryTables(final BgpRequestEntity<ServiceMyTableRequestDTO> request)
			throws BgpRestBusinessException;

	BgpResponseEntity<ServiceMyTableResponseDTO> queryTablesFallback(
			final BgpRequestEntity<ServiceMyTableResponseDTO> request, final Throwable throwable)
			throws BgpRestBusinessException;

	BgpResponseEntity<ServiceMyCatalogResponseDTO> queryCatalog(
			final BgpRequestEntity<ServiceMyCatalogRequestDTO> request) throws BgpRestBusinessException;

	BgpResponseEntity<ServiceMyCatalogResponseDTO> queryCatalogFallback(
			final BgpRequestEntity<ServiceMyCatalogResponseDTO> request, final Throwable throwable)
			throws BgpRestBusinessException;

	BgpResponseEntity<ServiceMyCatalogResponseDTO> updateCatalog(
			final BgpRequestEntity<ServiceMyCatalogUpdateRequestDTO> request) throws BgpRestBusinessException;

	BgpResponseEntity<ServiceMyCatalogResponseDTO> updateCatalogFallback(
			final BgpRequestEntity<ServiceMyCatalogUpdateRequestDTO> request, final Throwable throwable)
			throws BgpRestBusinessException;
}
