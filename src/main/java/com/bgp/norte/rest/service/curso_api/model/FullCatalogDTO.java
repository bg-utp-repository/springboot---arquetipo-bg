package com.bgp.norte.rest.service.curso_api.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Clase para datos de catalogo para administrar")
public class FullCatalogDTO implements Serializable {
	private static final long serialVersionUID = 2984285415706834698L;
	private String id;
	private String name;
	private String status;
	private String updated;
}
