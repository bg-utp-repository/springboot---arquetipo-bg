package com.bgp.norte.rest.service.curso_api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Clase para Productos de catalogo")
public class ServiceMyCatalogUpdateRequestDTO implements Serializable {

	private static final long serialVersionUID = 7145107480694739136L;
	private String table;
	private List<FullCatalogDTO> catalog;
}
