package com.bgp.norte.rest.service.curso_api.command.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgp.norte.rest.service.common.constant.ServiceResponseCode;
import com.bgp.norte.rest.service.common.entities.BgpRequestEntity;
import com.bgp.norte.rest.service.common.entities.BgpResponseEntity;
import com.bgp.norte.rest.service.common.exception.BgpRestBusinessException;
import com.bgp.norte.rest.service.common.model.StatusDTO;
import com.bgp.norte.rest.service.common.util.ResponseUtils;
import com.bgp.norte.rest.service.curso_api.command.Command;
import com.bgp.norte.rest.service.curso_api.generic.catalog.GenericQueryAuthorizationWsClient;
import com.bgp.norte.rest.service.curso_api.generic.catalog.GenericQueryTablesWsClient;
import com.bgp.norte.rest.service.curso_api.generic.catalog.model.GenericCatalogDTO;
import com.bgp.norte.rest.service.curso_api.generic.catalog.model.GenericRoleTableDTO;
import com.bgp.norte.rest.service.curso_api.generic.catalog.model.GenericTableDTO;
import com.bgp.norte.rest.service.curso_api.mapper.Mapper;
import com.bgp.norte.rest.service.curso_api.model.FullCatalogDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyCatalogRequestDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyCatalogResponseDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyCatalogUpdateRequestDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyTableRequestDTO;
import com.bgp.norte.rest.service.curso_api.model.ServiceMyTableResponseDTO;
import com.bgp.norte.ws.client.rest.generic.exception.BGPWsClientException;
import com.bgp.norte.ws.client.rest.generic.util.CommonUtils;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("Command")
public class CommandImpl implements Command {
	@Autowired
	private GenericQueryTablesWsClient genericQueryTablesWsClient;

	@Autowired
	private GenericQueryAuthorizationWsClient genericQueryAuthorizationWsClient;

	@Autowired
	private CommonUtils commonUtils;

	@Autowired
	private ResponseUtils responseUtils;

	@Autowired
	private Mapper mapper;

	@Override
	@HystrixCommand(commandKey = "queryTables", fallbackMethod = "queryTablesFallback")
	public BgpResponseEntity<ServiceMyTableResponseDTO> queryTables(final BgpRequestEntity<ServiceMyTableRequestDTO> request)
			throws BgpRestBusinessException {

		log.info("Ejecutando comando queryTables");

		final BgpResponseEntity<ServiceMyTableResponseDTO> response = new BgpResponseEntity<>();
		ServiceMyTableResponseDTO response2 = new ServiceMyTableResponseDTO();

		if (null != request) {
			try {
				response.setHeader(request.getHeader());
				List<GenericTableDTO> tablesResponseDTO = genericQueryTablesWsClient.queryTables(
						commonUtils.getGenericHeaderFromCommonHeader(request.getHeader()),
						request.getHeader().getNetworkUser());

				List<GenericRoleTableDTO> roleTablesResponseDTO = genericQueryAuthorizationWsClient.queryAuthorization(
						commonUtils.getGenericHeaderFromCommonHeader(request.getHeader()),
						request.getHeader().getNetworkUser());

				List<GenericTableDTO> result = tablesResponseDTO.stream().filter(
						n -> roleTablesResponseDTO.stream().anyMatch(n2 -> n2.getTabla().contains(n.getTabla().trim())))
						.collect(Collectors.toList());

				log.info(result.toString());

				response.setBody(mapper.fromCLTableDTOtoTableCatalogDTO(response2, result));
				response.setStatus(new StatusDTO(ServiceResponseCode.SUCCESS));

			} catch (BGPWsClientException e) {
				response.setStatus(commonUtils.getStatusFromGenericResponse(e.getGenericResponse()));
				log.error("Excepcion", e);
			}
		}

		return response;
	}

	@Override
	public BgpResponseEntity<ServiceMyTableResponseDTO> queryTablesFallback(
			final BgpRequestEntity<ServiceMyTableResponseDTO> request, final Throwable throwable)
			throws BgpRestBusinessException {
		log.warn("El flujo de la consulta <queryTablesFallback> se desvio al metodo fallback", throwable);

		final BgpResponseEntity<ServiceMyTableResponseDTO> response = new BgpResponseEntity<>();
		response.setStatus(responseUtils.genericFallBack(request, throwable).getStatus());

		return response;
	}

	@Override
	public BgpResponseEntity<ServiceMyCatalogResponseDTO> queryCatalog(
			BgpRequestEntity<ServiceMyCatalogRequestDTO> request) throws BgpRestBusinessException {
		log.info("Ejecutando comando queryCatalog");

		final BgpResponseEntity<ServiceMyCatalogResponseDTO> response = new BgpResponseEntity<>();

		if (null != request) {
			response.setHeader(request.getHeader());
			String networkUser = request.getHeader().getNetworkUser();
			String table = request.getBody().getTable();

			try {
				final List<GenericRoleTableDTO> autorized = genericQueryAuthorizationWsClient.validateAccessToTable(
						commonUtils.getGenericHeaderFromCommonHeader(request.getHeader()), table, networkUser);

				if (null != autorized && null != autorized.get(0) && null != autorized.get(0).getTabla()) {
					String tabla = autorized.get(0).getTabla();
					if (tabla.equalsIgnoreCase(table)) {
						final List<GenericCatalogDTO> genericList = genericQueryTablesWsClient.queryCatalog(
								commonUtils.getGenericHeaderFromCommonHeader(request.getHeader()), networkUser, table);

						final List<FullCatalogDTO> catalog = mapper.fromCLCatalogDTOtoFullCatalogDTO(genericList);
						response.setBody(new ServiceMyCatalogResponseDTO(table, catalog));
						response.setStatus(new StatusDTO(ServiceResponseCode.SUCCESS));
						return response;
					}
				} else {
					throw new BgpRestBusinessException("500",
							"No tiene autorización para administrar el catalogo " + table, networkUser);
				}
			} catch (BGPWsClientException e) {
				response.setStatus(commonUtils.getStatusFromGenericResponse(e.getGenericResponse()));
				log.error("Excepcion", e);
			}
		}

		return response;
	}

	@Override
	public BgpResponseEntity<ServiceMyCatalogResponseDTO> queryCatalogFallback(
			BgpRequestEntity<ServiceMyCatalogResponseDTO> request, Throwable throwable)
			throws BgpRestBusinessException {
		log.warn("El flujo de la consulta <queryCatalogFallback> se desvio al metodo fallback", throwable);

		final BgpResponseEntity<ServiceMyCatalogResponseDTO> response = new BgpResponseEntity<>();
		response.setStatus(responseUtils.genericFallBack(request, throwable).getStatus());

		return response;
	}

	@SuppressWarnings("unused")
	@Override
	public BgpResponseEntity<ServiceMyCatalogResponseDTO> updateCatalog(
			BgpRequestEntity<ServiceMyCatalogUpdateRequestDTO> request) throws BgpRestBusinessException {
		log.info("Ejecutando comando updateCatalog");

		Boolean result = false;
		final BgpResponseEntity<ServiceMyCatalogResponseDTO> response = new BgpResponseEntity<>();

		if (null != request) {
			response.setHeader(request.getHeader());
			String networkUser = request.getHeader().getNetworkUser();
			String table = request.getBody().getTable();

			try {
				final List<GenericRoleTableDTO> autorized = genericQueryAuthorizationWsClient.validateAccessToTable(
						commonUtils.getGenericHeaderFromCommonHeader(request.getHeader()), table, networkUser);

				if (null != autorized && null != autorized.get(0) && null != autorized.get(0).getTabla()) {
					for (FullCatalogDTO catalog : request.getBody().getCatalog()) {
						result = genericQueryTablesWsClient.updateCatalog(
								commonUtils.getGenericHeaderFromCommonHeader(request.getHeader()),
								request.getHeader().getNetworkUser(), catalog, table);
					}

					String tabla = autorized.get(0).getTabla();
					if (tabla.equalsIgnoreCase(table)) {
						final List<GenericCatalogDTO> genericList = genericQueryTablesWsClient.queryCatalog(
								commonUtils.getGenericHeaderFromCommonHeader(request.getHeader()), networkUser, table);
						final List<FullCatalogDTO> catalog = mapper.fromCLCatalogDTOtoFullCatalogDTO(genericList);
						response.setBody(new ServiceMyCatalogResponseDTO(table, catalog));
						response.setStatus(new StatusDTO(ServiceResponseCode.SUCCESS));
						return response;
					}
				} else {
					throw new BgpRestBusinessException("500",
							"No tiene autorización para administrar el catalogo " + table, networkUser);
				}
			} catch (BGPWsClientException e) {
				response.setStatus(commonUtils.getStatusFromGenericResponse(e.getGenericResponse()));
				log.error("Excepcion", e);
			}
		}

		return response;
	}

	@Override
	public BgpResponseEntity<ServiceMyCatalogResponseDTO> updateCatalogFallback(
			BgpRequestEntity<ServiceMyCatalogUpdateRequestDTO> request, Throwable throwable)
			throws BgpRestBusinessException {
		log.warn("El flujo de la consulta <updateCatalogFallback> se desvio al metodo fallback", throwable);

		final BgpResponseEntity<ServiceMyCatalogResponseDTO> response = new BgpResponseEntity<>();
		response.setStatus(responseUtils.genericFallBack(request, throwable).getStatus());

		return response;
	}
}
