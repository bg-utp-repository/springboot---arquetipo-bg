package com.bgp.norte.rest.service.curso_api.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Objeto que representa el cuerpo de la peticion de ejemplo")
public class ServiceRequestDTO implements Serializable {
  //TODO reemplazar el valor del campo serialVersionUID
  private static final long serialVersionUID = -7344177993138793816L;
  @ApiModelProperty(notes = "Mensaje de entrada", required = true, example = "Mensaje")
  @NotNull
  private String message;
}
