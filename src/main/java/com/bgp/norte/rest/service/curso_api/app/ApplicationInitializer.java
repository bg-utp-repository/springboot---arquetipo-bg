package com.bgp.norte.rest.service.curso_api.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import com.bgp.norte.rest.service.common.initializer.BgpApplicationRestInitializer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApplicationInitializer extends BgpApplicationRestInitializer {

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(ApplicationInitializer.class);
  }

  public static void main(String[] args) {
    log.info("Iniciando la aplicacion REST");
    SpringApplication.run(ApplicationInitializer.class, args);
  }
}
