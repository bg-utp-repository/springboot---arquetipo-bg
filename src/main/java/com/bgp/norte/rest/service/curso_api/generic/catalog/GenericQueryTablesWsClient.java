package com.bgp.norte.rest.service.curso_api.generic.catalog;

import java.util.List;

import com.bgp.norte.rest.service.curso_api.generic.catalog.model.GenericCatalogDTO;
import com.bgp.norte.rest.service.curso_api.generic.catalog.model.GenericTableDTO;
import com.bgp.norte.rest.service.curso_api.model.FullCatalogDTO;
import com.bgp.norte.ws.client.rest.generic.dto.HeaderDTO;
import com.bgp.norte.ws.client.rest.generic.exception.BGPWsClientException;

public interface GenericQueryTablesWsClient {

	String OPERATION_IU = "IU";
	String OPERATION_Q1 = "Q1";
	String OPERATION_Q2 = "Q2";
	String SP_BG_CL_CATALOGO = "sp_bg_cl_catalogo";

	List<GenericTableDTO> queryTables(final HeaderDTO header, String networkUser) throws BGPWsClientException;

	List<GenericCatalogDTO> queryCatalog(final HeaderDTO header, String networkUser, String table)
			throws BGPWsClientException;

	Boolean updateCatalog(final HeaderDTO header, String networkUser, FullCatalogDTO catalog, String table)
			throws BGPWsClientException;

}
