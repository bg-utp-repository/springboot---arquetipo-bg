package com.bgp.norte.rest.service.curso_api.generic.catalog.param;

import com.bgp.norte.ws.client.rest.generic.dto.SqlType;
import com.bgp.norte.ws.client.rest.generic.dto.WayParam;

import java.math.BigInteger;

public enum SPMaintenanceParam {

  // ==============
  // PARAMS
  // ==============
  OPERATION_TYPE("@i_operacion", SqlType.VARCHAR, 7, true, WayParam.IN),
  MODE("@i_modo", SqlType.NUMBER, 7, true, WayParam.IN),
  NETWORK_USER("@i_usuario_red", SqlType.VARCHAR, 7, true, WayParam.IN),
  TABLE("@i_catalogo_tabla", SqlType.VARCHAR, 7, true, WayParam.IN),
  ID("@i_codigo", SqlType.VARCHAR, 7, true, WayParam.IN),
  NAME("@i_valor", SqlType.VARCHAR, 7, true, WayParam.IN),
  STATUS("@i_estado", SqlType.VARCHAR, 7, true, WayParam.IN),
  ROW_COUNT("@i_rowcount", SqlType.NUMBER, 7, true, WayParam.IN),
  ROW_COUNT_OUT("@o_rowcount", SqlType.NUMBER, 7, true, WayParam.OUT);

  private String name;
  private SqlType sqlType;
  private BigInteger index;
  private boolean nillable;
  private WayParam mode;

  SPMaintenanceParam(String name, SqlType sqlType, int index, boolean nillable, WayParam mode) {
    this.name = name;
    this.sqlType = sqlType;
    this.index = BigInteger.valueOf(index);
    this.nillable = nillable;
    this.mode = mode;
  }

  public SqlType getSqlType() {
    return sqlType;
  }

  public BigInteger getIndex() {
    return index;
  }

  public boolean isNillable() {
    return nillable;
  }

  public WayParam getMode() {
    return mode;
  }

  public String getName() {
    return name;
  }
}
