package com.bgp.norte.rest.service.curso_api.generic.catalog.model;

import com.bgp.norte.ws.client.rest.generic.annotations.Column;
import com.bgp.norte.ws.client.rest.generic.dto.GenericClassDTO;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Clase para datos de catalogo para administrar")
public class GenericCatalogDTO extends GenericClassDTO implements Serializable {
  private static final long serialVersionUID = 8671238912L;

  @Column(name = "id")
  private String id;
  @Column(name = "name")
  private String name;
  @Column(name = "status")
  private String status;
}
