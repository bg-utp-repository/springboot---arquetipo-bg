package com.bgp.norte.rest.service.curso_api.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Objeto que representa el cuerpo de la respuesta de ejemplo")
public class ServiceResponseDTO implements Serializable {
  // TODO reemplazar el valor del campo serialVersionUID
  private static final long serialVersionUID = 8453122536334048510L;
  @ApiModelProperty(notes = "Mensaje de salida", required = false)
  private String message;

}
