package com.bgp.norte.rest.service.curso_api.generic.catalog.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgp.norte.rest.service.curso_api.generic.catalog.GenericQueryTablesWsClient;
import com.bgp.norte.rest.service.curso_api.generic.catalog.model.GenericCatalogDTO;
import com.bgp.norte.rest.service.curso_api.generic.catalog.model.GenericTableDTO;
import com.bgp.norte.rest.service.curso_api.generic.catalog.param.SPMaintenanceParam;
import com.bgp.norte.rest.service.curso_api.model.FullCatalogDTO;
import com.bgp.norte.ws.client.rest.generic.dto.HeaderDTO;
import com.bgp.norte.ws.client.rest.generic.dto.VendorDB;
import com.bgp.norte.ws.client.rest.generic.enums.ConnectionName;
import com.bgp.norte.ws.client.rest.generic.enums.DataBase;
import com.bgp.norte.ws.client.rest.generic.enums.GenericParams;
import com.bgp.norte.ws.client.rest.generic.exception.BGPWsClientException;
import com.bgp.norte.ws.client.rest.generic.service.GenericWsClient;
import com.bgp.norte.ws.client.rest.generic.util.HeaderUtils;
import com.bgp.norte.ws.client.rest.generic.util.ParameterUtil;
import com.bgp.norte.ws.client.rest.generic.util.ResponseUtil;

import generated.bg.EjecutarProcedimientoRequest;
import generated.bg.EjecutarProcedimientoResponse;
import lombok.extern.slf4j.Slf4j;

@Service("GenericQueryTablesWsClient")
@Slf4j
public class GenericQueryTablesWsClientImpl implements GenericQueryTablesWsClient {
	@Autowired
	private GenericWsClient genericWsClient;

	@Override
	public List<GenericTableDTO> queryTables(final HeaderDTO header, String networkUser) throws BGPWsClientException {
		log.info("ejecutando queryTables");

		List<GenericTableDTO> response = null;

		// Logica de invocacion al respectivo generico
		final EjecutarProcedimientoRequest sol = new EjecutarProcedimientoRequest();
		setConnection(sol);

		HeaderUtils.setLocalHeader(header);
		ParameterUtil.addParameter(GenericParams.OPERATION, OPERATION_Q2, sol);
		ParameterUtil.addParameter(SPMaintenanceParam.NETWORK_USER, networkUser, sol);

		try {
			final EjecutarProcedimientoResponse respuesta = genericWsClient.invokeGenericService(sol,
					SP_BG_CL_CATALOGO);
			response = ResponseUtil.mapResult(respuesta.getResultSet().get(0), GenericTableDTO.class);
		} catch (BGPWsClientException e) {
			log.error("Se ha presentado el siguiente error", e);
			throw e;
		}

		return response;
	}

	@Override
	public List<GenericCatalogDTO> queryCatalog(HeaderDTO header, String networkUser, String table)
			throws BGPWsClientException {
		log.info("ejecutando queryCatalog");

		List<GenericCatalogDTO> response = null;

		// Logica de invocacion al respectivo generico
		final EjecutarProcedimientoRequest sol = new EjecutarProcedimientoRequest();
		setConnection(sol);

		HeaderUtils.setLocalHeader(header);
		ParameterUtil.addParameter(GenericParams.OPERATION, OPERATION_Q1, sol);
		ParameterUtil.addParameter(SPMaintenanceParam.NETWORK_USER, networkUser, sol);
		ParameterUtil.addParameter(SPMaintenanceParam.TABLE, table, sol);

		try {
			final EjecutarProcedimientoResponse respuesta = genericWsClient.invokeGenericService(sol,
					SP_BG_CL_CATALOGO);
			response = ResponseUtil.mapResult(respuesta.getResultSet().get(0), GenericCatalogDTO.class);
		} catch (BGPWsClientException e) {
			log.error("Se ha presentado el siguiente error", e);
			throw e;
		}

		return response;
	}

	@Override
	public Boolean updateCatalog(HeaderDTO header, String networkUser, FullCatalogDTO catalog, String table)
			throws BGPWsClientException {
		log.info("ejecutando updateCatalog");

		// Logica de invocacion al respectivo generico
		final EjecutarProcedimientoRequest sol = new EjecutarProcedimientoRequest();
		setConnection(sol);

		HeaderUtils.setLocalHeader(header);
		ParameterUtil.addParameter(GenericParams.OPERATION, OPERATION_IU, sol);
		ParameterUtil.addParameter(SPMaintenanceParam.NETWORK_USER, networkUser, sol);

		try {
			if (null != table && null != catalog && null != catalog.getUpdated() && catalog.getUpdated().equals("S")) {
				log.info("ejecutando " + SP_BG_CL_CATALOGO);
				ParameterUtil.addParameter(SPMaintenanceParam.TABLE, table, sol);
				ParameterUtil.addParameter(SPMaintenanceParam.ID, catalog.getId(), sol);
				ParameterUtil.addParameter(SPMaintenanceParam.NAME, catalog.getName(), sol);
				ParameterUtil.addParameter(SPMaintenanceParam.STATUS, catalog.getStatus(), sol);

				genericWsClient.invokeGenericService(sol, SP_BG_CL_CATALOGO);
			}
		} catch (BGPWsClientException e) {
			log.error("Se ha presentado el siguiente error", e);
			throw e;
		}

		return true;
	}

	/**
	 * Set the connection name, data base and data base type.
	 *
	 * @param sol the new connection
	 */
	private void setConnection(EjecutarProcedimientoRequest sol) {
		sol.setConnectionName(ConnectionName.SYBASE_CONN_COBIS.value());
		sol.setDataBase(DataBase.COBIS.value());
		sol.setDataBaseType(VendorDB.SYBASE.value());
	}

}