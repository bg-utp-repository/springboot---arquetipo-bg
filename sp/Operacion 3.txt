Operacion 3: Actualizar un catalogo específico
1. Validar que el usuario pueda modificar el catalogo (utilizar validateAccessToTable)
		-- validateAccessToTable
		exec @w_return = sp_bg_findautorizedtable
		   @i_usuario_red    = "jllinares",
		   @i_operacion      = "Q2",
		   @i_formato        = 101,
		   @i_catalogo_tabla = "cc_sector",
		   @o_rowcount       = @w_rowcount out

2. Si tiene acceso, actualizar el detalle del catalogo (utilizar updateCatalog)
   El API debe ser capaz de procesar mas de un detalle al mismo tiempo.
   El API debe recibir la marca de que el detalle fue actualizado para procesarlo.
		-- queryCatalog
		exec @w_return = sp_bg_cl_catalogo
		   @s_user           = "6088",
		   @s_ofi            = 120,
		   @s_rol            = 10,
		   @s_websesion      = 135441,
		   @s_term           = "localhost",
		   @i_usuario_red    = "jllinares",
		   @i_operacion      = "Q1",
		   @i_formato        = 101,
		   @i_catalogo_tabla = "cc_sector",
		   @o_rowcount       = @w_rowcount out

3. Si tiene exito la actualización, devolver el catalogo completo como resultado (utilizar queryCatalog)
		-- queryCatalog
		exec @w_return = sp_bg_cl_catalogo
		   @s_user           = "6088",
		   @s_ofi            = 120,
		   @s_rol            = 10,
		   @s_websesion      = 135441,
		   @s_term           = "localhost",
		   @i_usuario_red    = "jllinares",
		   @i_operacion      = "Q1",
		   @i_formato        = 101,
		   @i_catalogo_tabla = "cc_sector",
		   @o_rowcount       = @w_rowcount out

Request del API:
{
  "header": {
    "networkUser": "jllinares",
    "office": 120,
    "role": 10,
    "terminal": "localhost",
    "uniqueNumber": 135441,
    "user": "6088"
  },
  "body": {
    "catalog": [
	  {
        "id": "P",
        "name": "PERSONAL",
        "status": "V",
        "updated": "S"
      },
      {
        "id": "R",
        "name": "REGIONAL",
        "status": "V",
        "updated": "S"
      },
      {
        "id": "T",
        "name": "TESORERIA",
        "status": "V",
        "updated": "S"
      }
    ],
    "table": "cc_sector"
  }
}

Response del API:
{
  "header": {
    "uniqueNumber": 135441,
    "office": 120,
    "role": 10,
    "terminal": "localhost",
    "user": "6088",
    "networkUser": "jllinares"
  },
  "status": {
    "code": "U0000",
    "description": "Ejecucion exitosa"
  },
  "body": {
    "table": "cc_sector",
    "catalog": [
      {
        "id": "C",
        "name": "COMERCIALES",
        "status": "V",
        "updated": "N"
      },
      {
        "id": "D",
        "name": "DISTINGUIDOS SUCURSALES",
        "status": "B",
        "updated": "N"
      },
      {
        "id": "E",
        "name": "EXTRANJERO LOCAL",
        "status": "B",
        "updated": "N"
      },
      {
        "id": "F",
        "name": "PREFERENCIAL",
        "status": "B",
        "updated": "N"
      },
      {
        "id": "G",
        "name": "CORPORATIVO",
        "status": "V",
        "updated": "N"
      },
      {
        "id": "H",
        "name": "HIPOTECARIO",
        "status": "V",
        "updated": "N"
      },
      {
        "id": "I",
        "name": "BANCA DE INVERSION",
        "status": "V",
        "updated": "N"
      },
      {
        "id": "MI",
        "name": "MI CATALOGO",
        "status": "V",
        "updated": "N"
      },
      {
        "id": "P",
        "name": "PERSONAL",
        "status": "V",
        "updated": "N"
      },
      {
        "id": "R",
        "name": "REGIONAL",
        "status": "V",
        "updated": "N"
      },
      {
        "id": "T",
        "name": "TESORERIA",
        "status": "V",
        "updated": "N"
      },
      {
        "id": "X",
        "name": "NUEVO",
        "status": "V",
        "updated": "N"
      }
    ]
  }
}